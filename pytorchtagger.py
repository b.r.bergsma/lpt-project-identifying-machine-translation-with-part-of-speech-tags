# based on the following tutorials:
#https://github.com/bentrevett/pytorch-sentiment-analysis/blob/master/1%20-%20Simple%20Sentiment%20Analysis.ipynb
#https://github.com/bentrevett/pytorch-sentiment-analysis/blob/master/2%20-%20Upgraded%20Sentiment%20Analysis.ipynb

import torch
from torchtext import data
from torchtext.vocab import Vectors
import torch.nn as nn
import torch.optim as optim
import time
import random

    
def count_parameters(model):
    """count number of trainable parameters"""
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

def calc_accuracy(preds, y):
    """returns accuracy per batch."""
    #round to closest integer, cause binary classification
    rounded_preds = torch.round(torch.sigmoid(preds))
    correct = (rounded_preds == y).float()
    acc = correct.sum() / len(correct)
    return acc
 
def train(model, train_iterator, optimizer, criterion):
    """train the model"""
    epoch_loss = 0
    epoch_acc = 0
    model.train() # activate  training
    for batch in train_iterator:
        optimizer.zero_grad() #reset gradient
        text, test_lengths = batch.text
        predictions = model(text, test_lengths).squeeze(1)
        loss = criterion(predictions, batch.label)
        acc = calc_accuracy(predictions, batch.label)
        loss.backward()
        optimizer.step()
        epoch_loss += loss.item()
        epoch_acc += acc.item()
    # return average loss and acc over all batches    
    return epoch_loss / len(train_iterator), epoch_acc / len(train_iterator)
    
def evaluate(model, valid_iterator, criterion):
    """evaluate the model (no updateing model) """
    epoch_loss = 0
    epoch_acc = 0
    model.eval() #deactivate training (no updating model)
    with torch.no_grad():
        for batch in valid_iterator:
            text, test_lengths = batch.text
            predictions = model(text, test_lengths).squeeze(1)
            loss = criterion(predictions, batch.label)
            acc = calc_accuracy(predictions, batch.label)
            epoch_loss += loss.item()
            epoch_acc += acc.item()
    return epoch_loss / len(valid_iterator), epoch_acc / len(valid_iterator)


def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs
    

class Classifier(nn.Module):
    """definition of the neural network model"""
    def __init__(self, input_dim, embedding_dim, hidden_dim, output_dim, n_layers, dropout, pad_idx): 
        super().__init__()
        self.embedding = nn.Embedding(input_dim, embedding_dim, padding_idx = pad_idx)
        self.rnn = nn.LSTM(embedding_dim, hidden_dim, bidirectional=True,
                           num_layers=n_layers, dropout=dropout)
        self.out = nn.Linear(hidden_dim * 2, output_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, text, text_lengths):
        #text = [sent len, batch size]
        
        embedded = self.dropout(self.embedding(text))
        #embedded = [sent len, batch size, emb dim]
        #packing ensured we do not process the the padded elements
        # this makes it faster
        packed_embedded = nn.utils.rnn.pack_padded_sequence(embedded, text_lengths)
        packed_output, (hidden, cell) = self.rnn(packed_embedded)
        
        #hidden = [num layers * num directions, batch size, hid dim]
        #cell = [num layers * num directions, batch size, hid dim]
        #concat the final forward (hidden[-2,:,:]) and backward (hidden[-1,:,:]) hidden layers
        #and apply dropout       
        
        hidden = self.dropout(torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim = 1))          
        #hidden = [batch size, hid dim * num directions]
        return self.out(hidden.squeeze(0))

#list with all words+postag from traindata
postags = []

def read_postags():
    """Read in all the word+postag from file into list"""
    postag_file = open("data/words+pos.de", "r")
    for line in postag_file:
        line = line.split("\t")
        word = line[0]
        tag = line[2]
        if tag[-1] == "\n":
            tag = tag[:-1]
        wordtag = word + tag
        postags.append(wordtag)

#global i is used to iterate over the postags list in the add_postags function
i = 0
def add_postags(x):
    """preprocessing-function to add word+postag to TEXT"""
    global i
    tagged_tokens = []
    for token in x:
        tagged_tokens.append(token)
        tagged_tokens.append(postags[i])
        i += 1
    x = tagged_tokens
    return x


print('preprocessing data')        
#preprocessing data using torchtext
read_postags()
# TEXT is preprocessed with added word+postags
TEXT = data.Field(include_lengths = True, preprocessing=add_postags)
LABEL = data.LabelField(dtype = torch.float)
POS = data.Field()

#seeed for reproducibility (same train/dev)
SEED = 753
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

datafields = [("text", TEXT),
             ("label", LABEL)]
dataset  = data.TabularDataset(
            path="data/data170k.csv",
            format="csv", fields=datafields)
         
train_data, valid_data = dataset.split(random_state = random.seed(SEED), split_ratio = 0.9)
vec = Vectors(name='cc.de.300.converted.vec', cache='data')
print(train_data[0].text)
print('building vocabulary')
TEXT.build_vocab(train_data, unk_init = torch.Tensor.normal_,
                 vectors = vec) # hier pretrained embeddins toevoegen met vector
LABEL.build_vocab(train_data)

#creating an instance of the model
INPUT_DIM = len(TEXT.vocab) #dimension of the one-hot vectors
EMBEDDING_DIM = 300 #should be same as pretrained
HIDDEN_DIM = 300 #often 100-500 dim
OUTPUT_DIM = 1 # the prediction
N_LAYERS = 2
DROPOUT = 0.5
PAD_IDX = TEXT.vocab.stoi[TEXT.pad_token]

print('creating model')
model = Classifier(INPUT_DIM,EMBEDDING_DIM,HIDDEN_DIM,OUTPUT_DIM,N_LAYERS,DROPOUT,PAD_IDX)
print(f'The model has {count_parameters(model):,} trainable parameters')
print('preparing training')

# for when we use pretrained embeddings
pretrained_embeddings = TEXT.vocab.vectors
print(pretrained_embeddings.shape)
model.embedding.weight.data.copy_(pretrained_embeddings)
UNK_IDX = TEXT.vocab.stoi[TEXT.unk_token]
model.embedding.weight.data[UNK_IDX] = torch.zeros(EMBEDDING_DIM)
model.embedding.weight.data[PAD_IDX] = torch.zeros(EMBEDDING_DIM)

BATCH_SIZE = 64

#GPU if possible
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

train_iterator, valid_iterator = data.BucketIterator.splits(
    (train_data, valid_data), batch_size = BATCH_SIZE, 
    device = device, sort_key=lambda x: len(x.text),
    sort_within_batch=True)

# prepare training
optimizer = optim.Adam(model.parameters())
criterion = nn.BCEWithLogitsLoss() #criterion is loss

#create possiblity of GPU
model = model.to(device)
criterion = criterion.to(device)

#training
EPOCHS = 3
best_valid_loss = float('inf')
for epoch in range(EPOCHS):
    start_time = time.time()
    train_loss, train_acc = train(model, train_iterator, optimizer, criterion)
    valid_loss, valid_acc = evaluate(model, valid_iterator, criterion)
    end_time = time.time()
    epoch_mins, epoch_secs = epoch_time(start_time, end_time)
    
    # if model has improved this epoch, keep model
    if valid_loss < best_valid_loss:
        best_valid_loss = valid_loss
        torch.save(model.state_dict(), 'baseline_testing_model.pt')

    print(f'Epoch: {epoch+1:02} | Epoch Time: {epoch_mins}m {epoch_secs}s')
    print(f'\tTrain Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}%')
    print(f'\t Val. Loss: {valid_loss:.3f} |  Val. Acc: {valid_acc*100:.2f}%')
print('best Val. Loss: {0}'.format(best_valid_loss))
