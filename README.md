DESCRIPTION

This system is usable for human/machine trans-lation classification, using a bi-LSTM neural net-work  build  with  PyTorch.   In  particular, the system has an added preprocessing function that adds PoS-tags to the trainable parameters.


REQUIREMENTS

Python3, Pytorch, Torchtext

TRAINING AND TESTING

To train and test with the system you have to execute the following command in the python shell:
"python3 pytorchtagger.py"

USED DATA

The system uses multiple external files. In the current repository the file with fasttext embeddings is missing. You can download the required file at https://drive.google.com/a/rug.nl/file/d/1mcxaMZm_2vwGtEJDZp_jTUVPv-jalqOd/view?usp=drivesdk (use your RUG-account for access) and place it in the 'data' folder.

AUTHORS

Patrick Plattje, Bastian Bergsma